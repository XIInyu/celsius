package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
	@Test
	public void testRegular(){
		assertTrue("Invalid Input", Celsius.convertFromFahrenheit(32)==0);
	}
	
	@Test
	public void testExceptional(){
		assertFalse("Invalid Input", Celsius.convertFromFahrenheit(0)==0);
	}
	
	@Test
	public void testBoundaryIn(){
		assertTrue("Invalid Input", Celsius.convertFromFahrenheit(5)==-15);
	}
	
	@Test
	public void testBoundaryOut(){
		assertFalse("Invalid Input", Celsius.convertFromFahrenheit(6)==-15);
	}

}
